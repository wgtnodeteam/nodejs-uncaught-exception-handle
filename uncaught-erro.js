var cluster = require('cluster');
var workers = process.env.WORKERS || require('os').cpus().length;

if (cluster.isMaster) {
  console.log('start cluster with %s workers', workers);
  for (var i = 0; i < workers; ++i) {
    var worker = cluster.fork().process;
    console.log('worker %s started.', worker.pid);
  }
  cluster.on('exit', function(worker) {
    console.log('worker %s died. restart...', worker.process.pid);
    cluster.fork();
  });
} else {
	var http = require('http');
	var fs = require('fs');
	//create a server object:
	http.createServer(function (req, res) {
		fs.readFile(__dirname+'/test.txt', function(err, data){
			if (err) throw err;
			console.log(data);
		});
	  res.write('Hello World!'); //write a response to the client
	  res.end(); //end the response
	}).listen(8080); //the server object listens on port 8080
}

process.on('uncaughtException', function (err) {
  console.error((new Date).toUTCString() + ' uncaughtException:', err.message)
  console.error(err.stack)
  process.exit(1)
})
